<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Auto translator',
    'description' => 'Base extension for auto translation',
    'category' => 'be',
    'author' => 'Kay Wienöbst',
    'author_email' => 'kay@hauptsache.net',
    'state' => 'stable',
    'clearCacheOnLoad' => true,
    'version' => '3.0.1',
    'constraints' => [
        'depends' => [
            'typo3' => '10.4.1-10.4.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
