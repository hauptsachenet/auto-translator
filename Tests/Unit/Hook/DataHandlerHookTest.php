<?php

declare(strict_types=1);

/**
 * This file is part of the "auto_translator" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace Hn\AutoTranslator\Tests\Unit\Hook;


use Hn\AutoTranslator\Hook\DataHandlerHook;
use Hn\AutoTranslator\Service\TranslationService;
use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * Class DataHandlerHookTest
 * @package Hn\AutoTranslator\Tests\Unit\Hook
 */
class DataHandlerHookTest extends UnitTestCase
{

    /**
     * @var DataHandler|MockObject
     */
    protected $dataHandlerMock;

    /**
     * @var TranslationService|MockObject
     */
    protected $dataHandlerServiceMock;

    /**
     * @var DataHandlerHook
     */
    protected $dataHandlerHook;

    protected function setUp(): void
    {
        $this->dataHandlerMock = $this->createMock(DataHandler::class);
        $this->dataHandlerServiceMock = $this->createMock(TranslationService::class);
        $this->dataHandlerHook = new DataHandlerHook($this->dataHandlerServiceMock);
    }

    public function testProcessTranslateTo_copyAction()
    {
        $content = 'content';

        $this->dataHandlerServiceMock
            ->expects(self::once())
            ->method('translateRecordField')
            ->with(
                self::equalTo('table'),
                self::equalTo(1),
                self::equalTo(2),
                self::equalTo($content)
            );

        $this->dataHandlerHook->processCmdmap_preProcess(null, 'table', '1', 'value', $this->dataHandlerMock, null);
        $this->dataHandlerHook->processTranslateTo_copyAction($content, ['uid' => '2'], $this->dataHandlerMock, 'fieldName');
    }

}
