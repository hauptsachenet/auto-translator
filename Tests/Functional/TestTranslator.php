<?php

declare(strict_types=1);

/**
 * This file is part of the "auto_translator" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace Hn\HnAutotranslator\Tests\Functional;


use Hn\AutoTranslator\Service\TranslatorInterface;

/**
 * Class TestTranslator
 * @package Hn\HnAutotranslator\Tests\Functional
 */
class TestTranslator implements TranslatorInterface
{

    public function translate(string $text, string $targetLanguageIsoCode, string $sourceLanguageIsoCode): string
    {
        return "translated '$text' from '$sourceLanguageIsoCode' to '$targetLanguageIsoCode'";
    }
}
