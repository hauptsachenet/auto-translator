<?php

declare(strict_types=1);

/**
 * This file is part of the "auto_translator" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace Hn\HnAutotranslator\Tests\Functional;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use Hn\AutoTranslator\Hook\DataHandlerHook;
use Hn\AutoTranslator\Service\TranslatorInterface;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Core\Bootstrap;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\Container\Container;
use TYPO3\TestingFramework\Core\Functional\FunctionalTestCase;

/**
 * Test case
 */
class TranslationTest extends FunctionalTestCase
{
    protected $testExtensionsToLoad = [
        'typo3conf/ext/auto_translator',
    ];

    protected $pathsToLinkInTestInstance = [
        'typo3conf/ext/auto_translator/Tests/Functional/Fixtures/sites' => 'typo3conf/sites'
    ];

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        if (!class_exists(TestTranslator::class)) {
            include(__DIR__ . '/TestTranslator.php');
        }
    }


    /**
     * Sets up this test case.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpBackendUserFromFixture(1);
        Bootstrap::initializeLanguageObject();

        GeneralUtility::makeInstance(Container::class)
            ->registerImplementation(
                TranslatorInterface::class,
                TestTranslator::class
            );

        $this->importDataSet(ORIGINAL_ROOT . 'typo3conf/ext/auto_translator/Tests/Functional/Fixtures/Dataset.xml');
    }

    public function provideTestProcessCmdmapClassHook()
    {
        // source language 'en' from site configuration
        yield ['1', 'localize', '1', "[Translate to Deutsch:] translated 'english home' from 'en' to 'de'"];
        yield ['1', 'copyToLanguage', '1', "[Translate to Deutsch:] translated 'english home' from 'en' to 'de'"];
        // source language 'de' from site configuration
        yield ['4', 'localize', '2', "[Translate to English:] translated 'german home' from 'de' to 'en'"];
        yield ['4', 'copyToLanguage', '2', "[Translate to English:] translated 'german home' from 'de' to 'en'"];
        // source language 'de' from record
        yield ['3', 'localize', '3', "[Translate to French:] translated 'english home subpage de' from 'de' to 'fr'"];
        yield ['3', 'copyToLanguage', '3', "[Translate to French:] translated 'english home subpage de' from 'de' to 'fr'"];
    }

    /**
     * @param $sourcePageUid
     * @param $targetSysLanguageUid
     * @param $translatedPageUid
     * @param $translatedPageTitle
     *
     * @dataProvider provideTestProcessCmdmapClassHook
     */
    public function testProcessCmdmapClassHook($sourcePageUid, $command, $targetSysLanguageUid, $expectedTranslatedPageTitle)
    {
        $dataHandler = GeneralUtility::makeInstance(DataHandler::class);
        $dataHandler->start([], [
            'pages' => [
                $sourcePageUid =>
                    [$command => $targetSysLanguageUid]
            ]
        ]);
        $dataHandler->process_cmdmap();
        $translatedPageRecord = (BackendUtility::getRecord('pages', 5));
        self::assertEquals($expectedTranslatedPageTitle, $translatedPageRecord['title']);
    }
}
