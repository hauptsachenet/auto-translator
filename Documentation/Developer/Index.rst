.. include:: ../Includes.txt

.. _developer:

================
Developer Corner
================

Providing a new translator is quite simple. Just create a new class that
implements the TranslatorInterface interface and register this class as an
implementation.


.. _developer-example:

EXAMPLE
=======

First create your translator class

.. code-block:: php

   namespace Acme\ExampleTranslator\Service;

   use Hn\AutoTranslator\Service\TranslatorInterface;

   class ExampleTranslator implements TranslatorInterface
   {
       /**
        * @param string $text the text to be translated
        * @param string $targetLanguageIsoCode the target language ISO 639-1 code
        * @param string $sourceLanguageIsoCode the source language ISO 639-1 code
        * @return string the translated text
        */
       public function translate(string $text, string $targetLanguageIsoCode, string $sourceLanguageIsoCode): string
       {
            // do your translation here. this example will only add a "example" prefix
           return 'example ' . $text;
       }
   }

Then register your class name as an implementation of the interface in your ext_localconf.php

.. code-block:: php

   \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\Container\Container::class)
        ->registerImplementation(
            \Hn\AutoTranslator\Service\TranslatorInterface::class,
            \Acme\ExampleTranslator\Service\ExampleTranslator::class
        );
