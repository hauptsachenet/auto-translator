.. include:: ../Includes.txt

.. _introduction:

============
Introduction
============

.. _what-it-does:

What does it do?
================

This extensions acts as a starting point for implementing automatic translation
of pages and content.

.. important::

   This extension does not provide any actual translation services.
