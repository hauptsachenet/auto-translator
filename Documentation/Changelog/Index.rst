.. include:: ../Includes.txt

.. _changelog:

==========
Change log
==========

Version 2.0.0
-------------

Complete rebuild of the extension. Added unit and functional test. added a
documentation
