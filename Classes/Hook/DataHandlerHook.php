<?php

declare(strict_types=1);

/**
 * This file is part of the "auto_translator" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace Hn\AutoTranslator\Hook;

use Hn\AutoTranslator\Service\TranslationService;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;

/**
 * Class DataHandlerHook
 *
 * use "processCmdmap_preProcess" hook in order to store the record table name and uid for "localize" and
 * "copyToLanguage" commands. We need this info to determine the source language for the translation process.
 * The source language is either derived directly from this record, if the sys_language_uid is greater than zero,
 * or, if it is zero, from the site configuration default language.
 *
 * @package Hn\AutoTranslator\Hook
 */
class DataHandlerHook implements SingletonInterface
{
    /**
     * Stores the current record being processed per date handler instance as [$table, $uid]
     *
     * @var array
     */
    protected $currentRecordInfo;

    /**
     * @var TranslationService
     */
    private $dataHandlerService;

    /**
     * DataHandler constructor.
     *
     * @param TranslationService|null $dataHandlerService dependency injection does not work for objects instantiated
     * via GeneralUtility::makeInstance(), but for testing purposes i allow this service to be injected
     */
    public function __construct(TranslationService $dataHandlerService = null)
    {
        if ($dataHandlerService === null) {
            $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
            $dataHandlerService = $objectManager->get(TranslationService::class);
        }
        $this->dataHandlerService = $dataHandlerService;
    }

    /**
     * hook method
     *
     * @param $command
     * @param $table
     * @param $id
     * @param $value
     * @param DataHandler $dataHandler
     * @param $pasteUpdate
     */
    public function processCmdmap_preProcess($command, $table, $id, $value, DataHandler $dataHandler, $pasteUpdate)
    {
        $this->currentRecordInfo[spl_object_hash($dataHandler)] = [$table, $id];
    }

    /**
     * hook method
     *
     * @param $content
     * @param $targetSysLanguageRecord
     * @param DataHandler $dataHandler
     * @param null $fieldName
     */
    public function processTranslateTo_copyAction(&$content, $targetSysLanguageRecord, DataHandler $dataHandler, $fieldName = null)
    {
        $currentRecordInfo = $this->currentRecordInfo[spl_object_hash($dataHandler)] ?? null;
        if ($currentRecordInfo !== null) {
            [$table, $uid] = $currentRecordInfo;
            $content = $this->dataHandlerService->translateRecordField($table, (int)$uid, (int)$targetSysLanguageRecord['uid'], $content);
        }
    }

}
