<?php

declare(strict_types=1);

/**
 * This file is part of the "auto_translator" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace Hn\AutoTranslator\Service;

/**
 * Interface TranslatorInterface
 * @package Hn\AutoTranslator\Service
 */
interface TranslatorInterface
{
    /**
     * @param string $text the text to be translated
     * @param string $targetLanguageIsoCode the target language ISO 639-1 code
     * @param string $sourceLanguageIsoCode the source language ISO 639-1 code
     * @return string the translated text
     */
    public function translate(string $text, string $targetLanguageIsoCode, string $sourceLanguageIsoCode): string;
}
