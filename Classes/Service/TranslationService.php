<?php

declare(strict_types=1);

/**
 * This file is part of the "auto_translator" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace Hn\AutoTranslator\Service;


use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Exception;
use TYPO3\CMS\Extbase\Object\ObjectManager;

/**
 * Class TranslationService
 * @package Hn\AutoTranslator\Service
 */
class TranslationService implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var SiteFinder
     */
    private $siteFinder;

    /**
     * DataHandlerService constructor.
     *
     * @param SiteFinder $siteFinder
     */
    public function __construct(SiteFinder $siteFinder)
    {
        $this->siteFinder = $siteFinder;
    }

    /**
     * Returns the registered implementation of the TranslatorInterface
     *
     * @return TranslatorInterface
     */
    public function getTranslator(): TranslatorInterface
    {
        /*
         * constructor injection is not used for the translator service, since there might not even be a registered
         * implementation. If that was the case it would cause an UnknownClassException when trying to instantiate this
         * service. That is why the object manager is used here in place to get the TranslatorInterface implementation
         */

        /** @var ObjectManager $objectManager */
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);

        /** @var TranslatorInterface $translator */
        $translator = $objectManager->get(TranslatorInterface::class);

        return $translator;
    }

    /**
     * @param $content
     * @param $targetSysLanguageRecord
     * @param DataHandler $dataHandler
     * @param null $fieldName
     * @return mixed|string
     */
    public function translateRecordField(string $recordTable, int $recordUid, int $targetSysLanguageUid, string $content): string
    {
        try {
            $targetLanguageIsoCode = $this->getSysLanguageIsoCode($targetSysLanguageUid);
            $sourceLanguageIsoCode = $this->getSourceLanguageIsocode($recordTable, $recordUid);
            $content = $this->getTranslator()->translate($content, $targetLanguageIsoCode, $sourceLanguageIsoCode);
        } catch (Exception $e) {
            $this->logger->critical($e->getMessage());
        }
        return $content;
    }

    /**
     * returns the source language ISO 639-1 code for the record in the current command info
     *
     * @param array $currentRecordInfo
     * @return string|null the language ISO 639-1 code
     * @throws \TYPO3\CMS\Core\Exception\SiteNotFoundException
     */
    protected function getSourceLanguageIsocode(string $table, int $uid): string
    {
        $sysLanguageUid = $this->getTranslatedRecordSysLanguageUid($table, $uid);

        // if the sys language uid is 0, look up the default language from the site configuration
        if ($sysLanguageUid === 0) {
            [$realPid] = BackendUtility::getTSCpid($table, $uid, '');

            $site = $this->siteFinder->getSiteByPageId($realPid);

            return $site->getDefaultLanguage()->getTwoLetterIsoCode();
        }

        return $this->getSysLanguageIsoCode($sysLanguageUid);
    }

    /**
     * @param int $uid
     * @return string
     */
    protected function getSysLanguageIsoCode(int $uid): string
    {
        $targetSysLanguageRecord = BackendUtility::getRecord('sys_language', $uid, 'language_isocode');
        return $targetSysLanguageRecord['language_isocode'];
    }

    /**
     * @param string $table
     * @param int $uid
     * @return int
     */
    protected function getTranslatedRecordSysLanguageUid(string $table, int $uid): int
    {
        $translatedRecord = BackendUtility::getRecord($table, $uid);

        return (int)$translatedRecord[$GLOBALS['TCA'][$table]['ctrl']['languageField']];
    }
}
